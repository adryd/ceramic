# Ceramic #
## What ##
Ceramic is a fork of Paper designed to implement some features from [CarpetMod](https://github.com/gnembon/carpetmod) and personal, non-game-changing improvements.

to setup dev env run
```
git submodule update --init --recursive
./concrete up
./concrete patch

# final jar
./concrete build 
```